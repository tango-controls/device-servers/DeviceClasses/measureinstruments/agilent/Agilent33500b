//+=============================================================================
//
// file :         LoadThread.h
//
// description :  Include for the LoadThread class.
//                This class is used for non blocking waveform loading
//
// project :      TANGO Device Server
//
// copyleft :     European Synchrotron Radiation Facility
//                BP 220, Grenoble 38043
//                FRANCE
//
//-=============================================================================

#ifndef AGILENT33500B_LOADTHREAD_H
#define AGILENT33500B_LOADTHREAD_H


// #include <tango.h>
// #include <iostream>
#include "Agilent33500B.h"

#define LOAD_WAVEFORM 1
#define LOAD_CONFIG   2

namespace Agilent33500B_ns
{

class LoadThread : public omni_thread, public Tango::LogAdapter {

public:
    // Constructor.
    LoadThread(Agilent33500B *, omni_mutex &);
    void run(void *);

private:

    omni_mutex &mutex;
    Agilent33500B *ds;


}; // class LoadThread
} // namespace Agilent33500B


#endif //AGILENT33500B_LOADTHREAD_H
