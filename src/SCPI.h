//=============================================================================
//
// file :        SCPI.h
//
// description : Handle SCPI protocol for Agilent 33500B over TCP/IP
//
// project :     Agilent 33500B device server
//
// This file is part of Tango device class.
// 
// Tango is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Tango is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Tango.  If not, see <http://www.gnu.org/licenses/>.
// 
// $Author: pons $
//
// $Log: SCPI.h,v $
//
//
//=============================================================================

#ifndef SCPIH
#define SCPIH

#include <string>

#define TRIG_FALLINGEDGE 0
#define TRIG_RISINGEDGE  1

#define TRIG_IMM 0
#define TRIG_EXT 1
#define TRIG_TIM 2
#define TRIG_BUS 3

#define BURST_TRIG 0
#define BURST_GAT  1

#define TRAC_ON  0
#define TRAC_OFF 1
#define TRAC_INV 2

#define FUNC_SIN      0
#define FUNC_SQUARE   1
#define FUNC_TRIANGLE 2
#define FUNC_RAMP     3
#define FUNC_PULSE    4
#define FUNC_PRB      5
#define FUNC_NOISE    6
#define FUNC_DC       7
#define FUNC_ARB      8

#define OUTPUT_NORM 0
#define OUTPUT_GAT  1

#define FILTER_NORMAL  0
#define FILTER_STEP    1
#define FILTER_OFF     2

class Scpi {

  public:

    Scpi(std::string SCPIServerName,int outputPort,int timeoutMilli,bool saveBootConfig);

    void HasTrack(bool flag) {hasTrack=flag;}

    bool GetIDN(std::string &idn);
    bool Reset();
    bool LoadWaveForm(int nb,double *waveform);
    bool ReadWaveForm(int *nb,double *waveform);

    bool SetOutput(int state);
    bool GetOutput(int *state);
    bool SetAmplitude(double amplitude);
    bool GetAmplitude(double *amplitude);
    bool SetOffset(double offset);
    bool GetOffset(double *offset);
    bool SetSampleRate(double sampleRate);
    bool GetSampleRate(double *sampleRate);
    bool SetTrigMode(int mode);
    bool GetTrigMode(int *mode);
    bool SetTrigEdge(int edge);
    bool GetTrigEdge(int *edge);
    bool SetBurstState(int state);
    bool GetBurstState(int *state);
    bool SetBurstMode(int mode);
    bool GetBurstMode(int *mode);
    bool SetTrackMode(int mode);
    bool GetTrackMode(int *mode);
    bool SetImpedance(double ohm);
    bool GetImpedance(double *ohm);
    bool SetBurstCycle(int nbCycle);
    bool GetBurstCycle(int *nbCycle);
    bool SetHighVoltage(double voltage,double *newAmp,double *newOff);
    bool GetHighVoltage(double *voltage);
    bool SetLowVoltage(double voltage,double *newAmp,double *newOff);
    bool GetLowVoltage(double *voltage);
    bool GetDelay(double *delay);
    bool SetDelay(double delay);
    bool GetFunc(int *func);
    bool SetFunc(int func);
    bool GetFreq(double *freq);
    bool SetFreq(double freq);
    bool GetNoiseBandwidth(double *bandwidth);
    bool SetNoiseBandwidth(double bandwidth);
    bool SetOutputMode(int mode);
    bool GetOutputMode(int *mode);
    bool SetWaveformFilter(int filter);
    bool GetWaveformFilter(int *filter);
    bool SetVPP();
    bool SetWAI();
    bool SetStateRecall();
    bool SetLimit(bool enable);
    bool SendSAV0();

    bool GetAll(int *state,
                double *amplitude,
                double *offset,
                double *sampleRate,
                short *trigMode,
                short *trigEdge,
                double *delay,
                short *burstState,
                short *burstMode,
                short *trackMode,
                double *ohm,
                short *nbCycle,
                double *highVoltage,
                double *lowVoltage,
                short *func,
                double *freq,
                double *noiseBandwidth,
                short *outputMode,
                short *waveformFilter);
		
    bool SetAll( double ohm,
                 double amplitude,
		 double offset,
		 double sampleRate,
		 int triggerMode,
		 int trigEdge,
		 double delay,
		 int trakcMode,
		 int burstMode,
		 int burstCycle,
		 int burstState,
		 int func,
		 double freq,
     double noiseBandwidth,
     short outputMode,
     short waveformFilter,
		 int nb,double *waveform);

    std::string GetLastError();

    bool is_connected () {
      return isConnected;
    }

  private:

    bool Connect();
    void Disconnect();
    int Write(char *buf, int bufsize,int timeout); // Timeout in millisec
    int Read(char *buf, int bufsize,int timeout);
    int WaitFor(int timeout,int mode);
    bool SendCommand(char *command,char *retBuffer = NULL,int maxLength = 0,int toWait=0,int cmdLength=0);
    time_t get_ticks();
    bool LoadWaveFormInternal(int nb,double *waveform);

    std::string SCPIServer;    
    std::string lastError;
    int output;
    int COMM_TIMEOUT;
    time_t tickStart;
    bool hasTrack;
    int sock;
    bool isConnected;
    bool saveBootConfig;

};

#endif /* SCPIH */
