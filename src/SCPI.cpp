//=============================================================================
//
// file :        SCPI.cpp
//
// description : Handle SCPI protocol for Agilent 33500B over TCP/IP
//
// project :     Agilent 33500B device server
//
// This file is part of Tango device class.
// 
// Tango is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Tango is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Tango.  If not, see <http://www.gnu.org/licenses/>.
// 
// $Author: pons $
//
// $Log: SCPI.cpp,v $
//
//
//=============================================================================

#include "SCPI.h"
#include <errno.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <netdb.h>
#include <netinet/tcp.h>
#include <iostream>
#include <math.h>
#include <string.h>

using namespace std;

#define WAIT_FOR_READ  1
#define WAIT_FOR_WRITE 2

#define closesocket close

#define SATURATE(x,min,max) if(x<min) x=min;if(x>max) x=max;

//#define SCPI_DEBUG 1

static double POSITIVE_INF = -log(0.0);

// ----------------------------------------------------------------------------

Scpi::Scpi(std::string SCPIServerName, int outputPort, int timeoutMilli,bool saveBootConfig) {

  SCPIServer = SCPIServerName;
  lastError = "";
  output = outputPort;
  COMM_TIMEOUT = timeoutMilli;
  tickStart = -1;
  hasTrack = true;
  isConnected = false;
  sock = -1;
  this->saveBootConfig = saveBootConfig;
  
}

// -------------------------------------------------------

int Scpi::WaitFor(int timeout,int mode) {

  fd_set fdset;
  fd_set *rd = NULL, *wr = NULL;
  struct timeval tmout;
  int result;

  FD_ZERO (&fdset);
  FD_SET (sock, &fdset);
  if (mode == WAIT_FOR_READ)
    rd = &fdset;
  if (mode == WAIT_FOR_WRITE)
    wr = &fdset;

  tmout.tv_sec  = (int)(timeout / 1000);
  tmout.tv_usec = (int)(timeout % 1000) * 1000;

  do
    result = select (sock + 1, rd, wr, NULL, &tmout);
  while (result < 0 && errno == EINTR);

  if( result==0 ) {
    lastError = "SCPI: The operation timed out";
  } else if ( result < 0 ) {
    lastError = "SCPI: Tranmission error";
    return 0;
  }

  return result;

}

// ----------------------------------------------------------------------------

int Scpi::Write(char *buf, int bufsize,int timeout) { // Timeout in millisec

  int total_written = 0;
  int written = 0;

  while( bufsize > 0 )
  {
    // Wait
    if (!WaitFor(timeout, WAIT_FOR_WRITE))
      return -1;

    // Write
    do
      written = send(sock, buf, bufsize, 0);
    while (written == -1 && errno == EINTR);

    if( written <= 0 )
       break;

    buf += written;
    total_written += written;
    bufsize -= written;
  }

  if( written < 0 ) {
    lastError = "SCPI: Tranmission error";
    return -1;
  }
  
  if( bufsize!=0 ) {
    lastError = "SCPI: Write invalid buffer length";
    return -1;
  }

  return total_written;

}

// ----------------------------------------------------------------------------

int Scpi::Read(char *buf, int bufsize,int timeout) { // Timeout in millisec

  int rd = 0;
  int total_read = 0;  
  
  //while( bufsize>0 ) {

    // Wait
    if (!WaitFor(timeout, WAIT_FOR_READ)) {
      return -1;
    }

    // Read
    do
      rd = recv(sock, buf, bufsize, 0);
    while (rd == -1 && errno == EINTR);

    //if( rd <= 0 )
    //  break;

    buf += rd;
    total_read += rd;
    bufsize -= rd;
  
  //}

  if( rd < 0 ) {
    lastError = "SCPI: Tranmission error";
    return -1;
  }
  
  
  return total_read;

}

// ----------------------------------------------------------------------------

void Scpi::Disconnect() {

  if(sock>=0)
    closesocket(sock);
  sock = -1;
  isConnected = false;

}

// ----------------------------------------------------------------------------

bool Scpi::Connect() {

  struct hostent *host_info;
  struct sockaddr_in server;  

  if(isConnected)
    return true;

  // Resolve IP
  host_info = gethostbyname(SCPIServer.c_str());
  if (host_info == NULL) {  
     lastError = "Unknown host: " + SCPIServer;
     return false; 
  }

  // Build TCP connection
  sock = socket(AF_INET, SOCK_STREAM,0);
  if (sock < 0 ) {
    switch(errno) {
      case EACCES:
        lastError = "SCPI: Socket error: Permission denied";
        break;
      case EMFILE:
        lastError = "SCPI: Socket error: Descriptor table is full";
        break;
      case ENOMEM:
        lastError = "SCPI: Socket error: Insufficient user memory is available";
        break;
      case ENOSR:
        lastError = "SCPI: Socket error: Insufficient STREAMS resources available";
        break;
      case EPROTONOSUPPORT:
        lastError = "SCPI: Socket error: Specified protocol is not supported";
        break;
      default:
        lastError = "SCPI: Socket error: Code:" + errno;
        break;
    }
    return false;
  }  

  // Connect
  memset(&server,0,sizeof(sockaddr_in));
  server.sin_family = host_info->h_addrtype;
  memcpy((char*)&server.sin_addr, host_info->h_addr,host_info->h_length);
  server.sin_port=htons(5025);
  if( connect(sock,(struct sockaddr *)&server, sizeof(server) ) < 0 ) {
    lastError = "Cannot connect to host: " + SCPIServer;
    Disconnect();
    return false;
  }
  
  int on = 1;
  if ( setsockopt (sock, SOL_SOCKET, SO_REUSEADDR, 
		   (const char*) &on, sizeof (on)) == -1) {
    lastError = "SCPI: Socket error: setsockopt error SO_REUSEADDR";
    Disconnect();
    return false; 
  }

  int flag = 1;
  struct protoent *p;
  p = getprotobyname("tcp");
  if ( setsockopt( sock, p->p_proto, TCP_NODELAY, (char *)&flag, sizeof(flag) ) == -1) {
    lastError = "SCPI: Socket error: setsockopt error TCP_NODELAY";
    Disconnect();
    return false; 
  }  
  
  isConnected = true;
  return true;

}

// ----------------------------------------------------------------------------

bool Scpi::SendCommand(char *command,char *retBuffer,int maxLength,int toWait,int cmdLength) {

  int nbRead=0;
  int nbSent=0;

  // Get the 50 first char of the command for error message
  char tmpCmd[50];
  strncpy(tmpCmd,command,50);
  tmpCmd[49]=0;

  // Send command    
#ifdef SCPI_DEBUG  
  time_t t0 = get_ticks();
  cout << "Write:" << command << endl;
#endif

  if(cmdLength==0) cmdLength = strlen(command);
  
  nbSent = Write(command,cmdLength,COMM_TIMEOUT);
  if( nbSent<0 ) {
    lastError += "\n" + string(tmpCmd);
    return false;
  }

  if(toWait>0) {
#ifdef SCPI_DEBUG  
    cout << "Wait " << toWait << " ms" << endl;
#endif
    usleep(toWait*1000);
  }
  
  if( maxLength<0 ) {
    // ABORT command which reset the stack
    return true;
  }
  
  // Read answer
  if( retBuffer ) {
    nbRead = Read(retBuffer,maxLength,COMM_TIMEOUT);
    if( nbRead<0 ) {
      // Read timeout
      lastError += "\n" + string(tmpCmd);
      return false;
    } 
    if(nbRead>=0 ) {  
      retBuffer[nbRead]=0;
#ifdef SCPI_DEBUG  
      cout << "Read: " << retBuffer << endl;
#endif  
    }
  }
  


#ifdef SCPI_DEBUG  
  time_t t1 = get_ticks();
  cout << command << " -> " << (t1-t0) << "ms" << endl;
#endif  
      
  // Check execution result
  char buffer[4096];
  
  // Send STB command  

#ifdef SCPI_DEBUG  
  cout << "Write: *STB?" << endl;
#endif  

  sprintf(buffer,"*STB?\n");
  nbSent = Write(buffer,strlen(buffer),COMM_TIMEOUT);
  if( nbSent<0 ) {
    lastError += "\n*STB? for " + string(tmpCmd);
    return false;
  }
  
  // Read answer
  nbRead = Read(buffer,4096,COMM_TIMEOUT);
  if( nbRead<0 ) {
    lastError += "\n*STB? for " + string(tmpCmd);
    return false;
  }
  
  buffer[nbRead]=0;

#ifdef SCPI_DEBUG  
  cout << "Read STB: " << buffer << endl;
#endif  
  
  lastError = "";
  
  int stb = atoi(buffer);
    
  if( (stb & 4)!=0 ) {
  
    // Error present in queue
    // Read the error queue
    
    bool eof = false;
    
    while(!eof) {
    
      sprintf(buffer,"SYST:ERR?\n");
      nbSent = Write(buffer,strlen(buffer),COMM_TIMEOUT);  
      if( nbSent<0 ) {
         lastError += " 'SYST:ERR?'";
         return false;
      }
      
      // Read answer
      nbRead = Read(buffer,4096,COMM_TIMEOUT);
      if( nbRead<0 ) {
        lastError += "\nSYST:ERR? for " + string(tmpCmd);
        return false;
      }
      
      buffer[nbRead]=0;
      
      eof = (strlen(buffer)==0) || (strncmp(buffer,"+0",2)==0);
      
      if(!eof) {
#ifdef SCPI_DEBUG  
        cout << "Read Error: " << buffer << endl;
#endif  
        lastError += string(buffer) + "\n";
      }
    
    }
    
    lastError += string(tmpCmd);
        
    return false;
    
  }
  
  return true;
  
}

// ----------------------------------------------------------------------------
bool Scpi::GetAll(int *state,
                  double *amplitude,
		  double *offset,
		  double *sampleRate,
		  short *trigMode,
		  short *trigEdge,
		  double *delay,
		  short *burstState,
		  short *burstMode,
		  short *trackMode,
		  double *ohm,
		  short *nbCycle,
		  double *highVoltage,
		  double *lowVoltage,
		  short  *func,
      double *freq,
      double *noiseBandwidth,
      short *outputMode,
      short *waveformFilter
) {

  char cmd[256];
  char buffer[4096];

  // Connect
  if( !Connect() ) {
    return false;
  }

  // Send OUTPUT? command
  sprintf(cmd,"OUTPUT%d?\n",output);
  if( !SendCommand(cmd,buffer,4096) ) {
    Disconnect();
    return false;
  }
  
  *state = atoi(buffer);
  
  // Send VOLT? command
  sprintf(cmd,"SOUR%d:VOLT?\n",output);
  if( !SendCommand(cmd,buffer,4096) ) {
    Disconnect();
    return false;
  }
  
  *amplitude = atof(buffer);

  // Send OFFS? command
  sprintf(cmd,"SOUR%d:VOLT:OFFS?\n",output);
  if( !SendCommand(cmd,buffer,4096) ) {
    Disconnect();
    return false;
  }
  
  *offset = atof(buffer);

  // Send SRAT? command
  sprintf(cmd,"SOUR%d:FUNC:ARB:SRAT?\n",output);
  if( !SendCommand(cmd,buffer,4096) ) {
    Disconnect();
    return false;
  }
  
  *sampleRate = atof(buffer);
  
  // Send TRIG? command
  sprintf(buffer,"TRIG%d:SOUR?\n",output);
  if( !SendCommand(buffer,buffer,4096) ) {
    Disconnect();
    return false;
  }
  
  if( strncasecmp(buffer,"IMM",3)==0 ) {
    *trigMode = TRIG_IMM;
  } else if( strncasecmp(buffer,"EXT",3)==0 ) {
    *trigMode = TRIG_EXT;
  } else if( strncasecmp(buffer,"TIM",3)==0 ) {
    *trigMode = TRIG_TIM;
  } else if( strncasecmp(buffer,"BUS",3)==0 ) {
    *trigMode = TRIG_BUS;
  } else {
    *trigMode = -1;
  }
  
  // Send TRIG? command
  sprintf(buffer,"TRIG%d:SLOP?\n",output);
  if( !SendCommand(buffer,buffer,4096) ) {
    Disconnect();
    return false;
  }
  
  if( strncasecmp(buffer,"POS",3)==0 ) {
    *trigEdge = TRIG_RISINGEDGE;
  } else if( strncasecmp(buffer,"NEG",3)==0 ) {
    *trigEdge = TRIG_FALLINGEDGE;
  } else {
    *trigEdge = -1;
  }

  // Send DEL? command
  sprintf(buffer,"TRIG%d:DEL?\n",output);
  if( !SendCommand(buffer,buffer,4096) ) {
    Disconnect();
    return false;
  }

  *delay = atof(buffer)*1e6;
  
  // Send BURST? command
  sprintf(buffer,"SOUR%d:BURST:STAT?\n",output);
  if( !SendCommand(buffer,buffer,4096) ) {
    Disconnect();
    return false;
  }
  
  *burstState = atoi(buffer);

  // Send BURST? command
  sprintf(buffer,"SOUR%d:BURST:MODE?\n",output);
  if( !SendCommand(buffer,buffer,4096) ) {
    Disconnect();
    return false;
  }
  
  if( strncasecmp(buffer,"TRIG",4)==0 ) {
    *burstMode = BURST_TRIG;
  } else if( strncasecmp(buffer,"GAT",3)==0 ) {
    *burstMode = BURST_GAT;
  } else {
    *burstMode = -1;
  }
  
  if( hasTrack ) {

    // Send TRAC? command
    sprintf(buffer,"SOUR%d:TRAC?\n",output);
    if( !SendCommand(buffer,buffer,4096) ) {
      Disconnect();
      return false;
    }
  
    if( strncasecmp(buffer,"ON",2)==0 ) {
      *trackMode = TRAC_ON;
    } else if( strncasecmp(buffer,"OFF",3)==0 ) {
      *trackMode = TRAC_OFF;
    } else if( strncasecmp(buffer,"INV",3)==0 ) {
      *trackMode = TRAC_INV;
    } else {
      *trackMode = -1;
    }

  } else {
    *trackMode = TRAC_OFF;
  }
  
  // Send LOAD? command
  sprintf(buffer,"OUTPUT%d:LOAD?\n",output);
  if( !SendCommand(buffer,buffer,4096) ) {
    Disconnect();
    return false;
  }
  
  *ohm = atof(buffer);
  if(*ohm>10000.0) *ohm = POSITIVE_INF;
  
  // Send NCYC? command
  sprintf(buffer,"SOUR%d:BURST:NCYC?\n",output);
  if( !SendCommand(buffer,buffer,4096) ) {
    Disconnect();
    return false;
  }
  
  *nbCycle = (int)(atof(buffer)+0.5);
  
  // Send VOLT:HIGH? command
  sprintf(buffer,"SOUR%d:VOLT:HIGH?\n",output);
  if( !SendCommand(buffer,buffer,4096) ) {
    Disconnect();
    return false;
  }
  
  *highVoltage = atof(buffer);

  // Send VOLT:LOW? command
  sprintf(buffer,"SOUR%d:VOLT:LOW?\n",output);
  if( !SendCommand(buffer,buffer,4096) ) {
    Disconnect();
    return false;
  }
  
  *lowVoltage = atof(buffer);

  // Send FUNC? command
  sprintf(buffer,"SOUR%d:FUNC?\n",output);
  if( !SendCommand(buffer,buffer,4096) ) {
    Disconnect();
    return false;
  }

  if(strncasecmp(buffer,"SIN",3)==0) {
    *func = FUNC_SIN;
  } else if(strncasecmp(buffer,"SQU",3)==0) {
    *func = FUNC_SQUARE;
  } else if(strncasecmp(buffer,"TRI",3)==0) {
    *func = FUNC_TRIANGLE;
  } else if(strncasecmp(buffer,"RAM",3)==0) {
    *func = FUNC_RAMP;
  } else if(strncasecmp(buffer,"PUL",3)==0) {
    *func = FUNC_PULSE;
  } else if(strncasecmp(buffer,"PRB",3)==0) {
    *func = FUNC_PRB;
  } else if(strncasecmp(buffer,"NOI",3)==0) {
    *func = FUNC_NOISE;
  } else if(strncasecmp(buffer,"DC",2)==0) {
    *func = FUNC_DC;
  } else if(strncasecmp(buffer,"ARB",3)==0) {
    *func = FUNC_ARB;
  } else {
    *func = -1;
  }

  // Send FREQ? command
  sprintf(cmd,"SOUR%d:FREQ?\n",output);
  if( !SendCommand(cmd,buffer,4096) ) {
    Disconnect();
    return false;
  }
  
  *freq = atof(buffer);

  // Send NOIS:BAND? command
  sprintf(cmd,"SOUR%d:FUNC:NOIS:BAND?\n",output);
  if( !SendCommand(cmd,buffer,4096) ) {
    Disconnect();
    return false;
  }

  *noiseBandwidth = atof(buffer);

  // Send OUT:MODE? command
  sprintf(buffer,"OUTP%d:MODE?\n",output);
  if( !SendCommand(buffer,buffer,4096) ) {
    Disconnect();
    return false;
  }

  if( strncasecmp(buffer,"NORM",4)==0 ) {
    *outputMode = OUTPUT_NORM;
  } else if( strncasecmp(buffer,"GAT",3)==0 ) {
    *outputMode = OUTPUT_GAT;
  } else {
    *outputMode = -1;
  }

  // Send SOUR:FUNC:ARB:FILT? command
  sprintf(buffer,"SOUR%d:FUNC:ARB:FILT?\n",output);
  if( !SendCommand(buffer,buffer,4096) ) {
    Disconnect();
    return false;
  }

  if( strncasecmp(buffer,"NORM",4)==0 ) {
    *waveformFilter = FILTER_NORMAL;
  } else if( strncasecmp(buffer,"STEP",4)==0 ) {
    *waveformFilter = FILTER_STEP;
  } else if( strncasecmp(buffer,"OFF",3)==0 ) {
    *waveformFilter = FILTER_OFF;
  } else {
    *waveformFilter = -1;
  }

  return true;

}

// ----------------------------------------------------------------------------

bool Scpi::SendSAV0() {

  if( saveBootConfig ) {
    char cmd[256];
    strcpy(cmd, "*SAV 0\n");
    if (!SendCommand(cmd)) {
      Disconnect();
      return false;
    }
  }

  return true;

}

// ----------------------------------------------------------------------------

bool Scpi::SetLimit(bool enable) {

    char cmd[256];

    // Connect
    if( !Connect() ) {
      return false;
    }

    sprintf(cmd,"VOLT:LIMIT:STATE %d\n",enable);

    if( !SendCommand(cmd) ) {
      Disconnect();
      return false;
    }

    return true;

}

// ----------------------------------------------------------------------------

bool Scpi::SetAll(double ohm,
                  double amplitude,
                  double offset,
                  double sampleRate,
                  int triggerMode,
                  int trigEdge,
                  double delay,
                  int trakcMode,
                  int burstMode,
                  int burstCycle,
                  int burstState,
                  int func,
                  double freq,
                  double noiseBandwidth,
                  short outputMode,
                  short waveformFilter,
                  int nb, double *waveform) {


  char cmd[256];

  // Connect
  if (!Connect()) {
    return false;
  }

  // Force VPP
  sprintf(cmd, "SOUR%d:VOLT:UNIT VPP\n", output);

  // Waveform
  if (!LoadWaveFormInternal(nb, waveform)) {
    Disconnect();
    return false;
  }

  // impedance (Send LOAD command)
  if (ohm <= 10000) {
    sprintf(cmd, "OUTPUT%d:LOAD %g\n", output, ohm);
  } else {
    sprintf(cmd, "OUTPUT%d:LOAD INF\n", output);
  }
  if (!SendCommand(cmd)) {
    Disconnect();
    return false;
  }

  // Use APPLY to apply VOLT,OFFS and SRAT and Ignore ERROR !!!
  //sprintf(cmd,"SOUR%d:APPL:ARB %g,%.3f,%.3f\n",output,sampleRate,amplitude,offset);
  //SendCommand(cmd);

  // Amplitude (Send VOLT command)
  sprintf(cmd, "SOUR%d:VOLT %.3f\n", output, amplitude);
  if (!SendCommand(cmd)) {
    Disconnect();
    return false;
  }

  // Offset (Send OFFS command)
  sprintf(cmd, "SOUR%d:VOLT:OFFS %.3f\n", output, offset);
  if (!SendCommand(cmd)) {
    Disconnect();
    return false;
  }

  // SampleRate (Send SRAT command)
  sprintf(cmd, "SOUR%d:FUNC:ARB:SRAT %g\n", output, sampleRate);
  if (!SendCommand(cmd)) {
    Disconnect();
    return false;
  }

  // triggerMode (Send TRIG command)
  switch (triggerMode) {

    case TRIG_IMM:
      sprintf(cmd, "TRIG%d:SOUR IMM\n", output);
      break;
    case TRIG_EXT:
      sprintf(cmd, "TRIG%d:SOUR EXT\n", output);
      break;
    case TRIG_TIM:
      sprintf(cmd, "TRIG%d:SOUR TIM\n", output);
      break;
    case TRIG_BUS:
      sprintf(cmd, "TRIG%d:SOUR BUS\n", output);
      break;
    default:
      lastError = "Invalid trigger mode";
      Disconnect();
      return false;

  }

  if (!SendCommand(cmd)) {
    Disconnect();
    return false;
  }

  // TrigEdge (Send TRIG command)
  switch (trigEdge) {

    case TRIG_FALLINGEDGE:
      sprintf(cmd, "TRIG%d:SLOP NEG\n", output);
      break;
    case TRIG_RISINGEDGE:
      sprintf(cmd, "TRIG%d:SLOP POS\n", output);
      break;
    default:
      lastError = "Invalid trigger edge";
      Disconnect();
      return false;

  }

  if (!SendCommand(cmd)) {
    Disconnect();
    return false;
  }

  // Set delay (in us)
  sprintf(cmd, "TRIG%d:DEL %g\n", output, delay / 1e6);
  if (!SendCommand(cmd)) {
    Disconnect();
    return false;
  }


  // trakcMode (Send TRAC command)
  if (hasTrack) {

    switch (trakcMode) {

      case TRAC_ON:
        sprintf(cmd, "SOUR%d:TRAC ON\n", output);
        break;
      case TRAC_OFF:
        sprintf(cmd, "SOUR%d:TRAC OFF\n", output);
        break;
      case TRAC_INV:
        sprintf(cmd, "SOUR%d:TRAC INV\n", output);
        break;
      default:
        lastError = "Invalid track mode";
        Disconnect();
        return false;

    }

    if (!SendCommand(cmd)) {
      Disconnect();
      return false;
    }

  }

  // burstMode (Send BURST command)
  switch (burstMode) {

    case BURST_TRIG:
      sprintf(cmd, "SOUR%d:BURST:MODE TRIG\n", output);
      break;
    case BURST_GAT:
      sprintf(cmd, "SOUR%d:BURST:MODE GAT\n", output);
      break;
    default:
      lastError = "Invalid burst mode";
      Disconnect();
      return false;

  }

  if (!SendCommand(cmd)) {
    Disconnect();
    return false;
  }

  // burstCycle (Send NCYC command)
  sprintf(cmd, "SOUR%d:BURST:NCYC %d\n", output, burstCycle);
  if (!SendCommand(cmd)) {
    Disconnect();
    return false;
  }

  // burstState (Send BURST command)
  sprintf(cmd, "SOUR%d:BURST:STAT %d\n", output, burstState);
  if (!SendCommand(cmd)) {
    Disconnect();
    return false;
  }

  // frequency (Send FREQ command)
  sprintf(cmd, "SOUR%d:FREQ %g\n", output, freq);
  if (!SendCommand(cmd)) {
    Disconnect();
    return false;
  }

  // noise bandwidth (Send NOIS:BAND command)
  sprintf(cmd, "SOUR%d:FUNC:NOIS:BAND %g\n", output, noiseBandwidth);
  if (!SendCommand(cmd)) {
    Disconnect();
    return false;
  }

  // FUNC

  if (func != FUNC_ARB) {

    switch (func) {
      case FUNC_SIN:
        sprintf(cmd, "SOUR%d:FUNC SIN\n", output);
        break;
      case FUNC_SQUARE:
        sprintf(cmd, "SOUR%d:FUNC SQU\n", output);
        break;
      case FUNC_TRIANGLE:
        sprintf(cmd, "SOUR%d:FUNC TRI\n", output);
        break;
      case FUNC_RAMP:
        sprintf(cmd, "SOUR%d:FUNC RAMP\n", output);
        break;
      case FUNC_PULSE:
        sprintf(cmd, "SOUR%d:FUNC PULS\n", output);
        break;
      case FUNC_PRB:
        sprintf(cmd, "SOUR%d:FUNC PRBS\n", output);
        break;
      case FUNC_NOISE:
        sprintf(cmd, "SOUR%d:FUNC NOIS\n", output);
        break;
      case FUNC_DC:
        sprintf(cmd, "SOUR%d:FUNC DC\n", output);
        break;
      default:
        lastError = "Invalid func mode";
        Disconnect();
        return false;
    }

    if (!SendCommand(cmd)) {
      Disconnect();
      return false;
    }

  }

  // Output Mode
  switch (outputMode) {

    case OUTPUT_NORM:
      sprintf(cmd, "OUTP%d:MODE NORM\n", output);
      break;
    case OUTPUT_GAT:
      sprintf(cmd, "OUTP%d:MODE GAT\n", output);
      break;
    default:
      lastError = "Invalid output mode";
      Disconnect();
      return false;

  }
  if (!SendCommand(cmd)) {
    Disconnect();
    return false;
  }

  // Filter Mode
  switch (waveformFilter) {

    case FILTER_NORMAL:
      sprintf(cmd, "SOUR%d:FUNC:ARB:FILT NORM\n", output);
      break;
    case FILTER_STEP:
      sprintf(cmd, "SOUR%d:FUNC:ARB:FILT STEP\n", output);
      break;
    case FILTER_OFF:
      sprintf(cmd, "SOUR%d:FUNC:ARB:FILT OFF\n", output);
      break;
    default:
      lastError = "Invalid output mode";
      Disconnect();
      return false;

  }
  if (!SendCommand(cmd)) {
    Disconnect();
    return false;
  }

  return SendSAV0();;

}

// ----------------------------------------------------------------------------
bool Scpi::SetVPP() {

  char cmd[256];

  // Connect
  if( !Connect() ) {
    return false;
  }
  
  sprintf(cmd,"SOUR%d:VOLT:UNIT VPP\n",output);
  
  if( !SendCommand(cmd) ) {
    Disconnect();
    return false;
  }  
  
  return true;
}

// ----------------------------------------------------------------------------

bool Scpi::SetStateRecall() {

  // Connect
  if( !Connect() ) {
    return false;
  }

  // Use SAV 0 when device is switched on
  if( !SendCommand((char *)"MEM:STAT:REC:AUTO ON\n") ) {
    Disconnect();
    return false;
  }

  return true;

}

// ----------------------------------------------------------------------------
bool Scpi::SetWAI() {


  // Connect
  if( !Connect() ) {
    return false;
  }
  
  if( !SendCommand((char *)"*WAI\n",NULL,-1) ) {
    Disconnect();
    return false;
  }
    
  return true;  
}

// ----------------------------------------------------------------------------
bool Scpi::SetTrigMode(int mode) {

  char cmd[256];

  // Connect
  if( !Connect() ) {
    return false;
  }
  
  // Send TRIG command
  switch( mode ) {

    case TRIG_IMM:
      sprintf(cmd,"TRIG%d:SOUR IMM\n",output);
      break;
    case TRIG_EXT:
      sprintf(cmd,"TRIG%d:SOUR EXT\n",output);
      break;
    case TRIG_TIM:
      sprintf(cmd,"TRIG%d:SOUR TIM\n",output);
      break;
    case TRIG_BUS:
      sprintf(cmd,"TRIG%d:SOUR BUS\n",output);
      break;
    default:
      lastError = "Invalid trigger mode";
      Disconnect();
      return false;
    
  }
  
  if( !SendCommand(cmd) ) {
    Disconnect();
    return false;
  }  
  
  return SendSAV0();

}

// ----------------------------------------------------------------------------
bool Scpi::GetTrigMode(int *mode) {

  char buffer[4096];

  // Connect
  if( !Connect() ) {
    return false;
  }
  
  // Send TRIG? command
  sprintf(buffer,"TRIG%d:SOUR?\n",output);
  if( !SendCommand(buffer,buffer,4096) ) {
    Disconnect();
    return false;
  }
  
  if( strncasecmp(buffer,"IMM",3)==0 ) {
    *mode = TRIG_IMM;
  } else if( strncasecmp(buffer,"EXT",3)==0 ) {
    *mode = TRIG_EXT;
  } else if( strncasecmp(buffer,"TIM",3)==0 ) {
    *mode = TRIG_TIM;
  } else if( strncasecmp(buffer,"BUS",3)==0 ) {
    *mode = TRIG_BUS;
  } else {
    *mode = -1;
  }
    
  return true;  

}

// ----------------------------------------------------------------------------
bool Scpi::SetTrigEdge(int edge) {

  char cmd[256];

  // Connect
  if( !Connect() ) {
    return false;
  }
  
  // Send TRIG command
  switch( edge ) {

    case TRIG_FALLINGEDGE:
      sprintf(cmd,"TRIG%d:SLOP NEG\n",output);
      break;
    case TRIG_RISINGEDGE:
      sprintf(cmd,"TRIG%d:SLOP POS\n",output);
      break;
    default:
      lastError = "Invalid trigger edge";
      Disconnect();
      return false;
    
  }
  
  if( !SendCommand(cmd) ) {
    Disconnect();
    return false;
  }  
  
  return SendSAV0();

}

// ----------------------------------------------------------------------------
bool Scpi::GetTrigEdge(int *edge) {

  char buffer[4096];

  // Connect
  if( !Connect() ) {
    return false;
  }
  
  // Send TRIG? command
  sprintf(buffer,"TRIG%d:SLOP?\n",output);
  if( !SendCommand(buffer,buffer,4096) ) {
    Disconnect();
    return false;
  }
  
  if( strncasecmp(buffer,"POS",3)==0 ) {
    *edge = TRIG_RISINGEDGE;
  } else if( strncasecmp(buffer,"NEG",3)==0 ) {
    *edge = TRIG_FALLINGEDGE;
  } else {
    *edge = -1;
  }
    
  return true;  

}

// ----------------------------------------------------------------------------
bool Scpi::SetBurstMode(int mode) {

  char cmd[256];

  // Connect
  if( !Connect() ) {
    return false;
  }
  
  // Send BURST command
  switch( mode ) {

    case BURST_TRIG:
      sprintf(cmd,"SOUR%d:BURST:MODE TRIG\n",output);
      break;
    case BURST_GAT:
      sprintf(cmd,"SOUR%d:BURST:MODE GAT\n",output);
      break;
    default:
      lastError = "Invalid burst mode";
      Disconnect();
      return false;
    
  }
  
  if( !SendCommand(cmd) ) {
    Disconnect();
    return false;
  }  
  
  return SendSAV0();

}

// ----------------------------------------------------------------------------
bool Scpi::GetBurstMode(int *mode) {

  char buffer[4096];

  // Connect
  if( !Connect() ) {
    return false;
  }
  
  // Send BURST? command
  sprintf(buffer,"SOUR%d:BURST:MODE?\n",output);
  if( !SendCommand(buffer,buffer,4096) ) {
    Disconnect();
    return false;
  }
  
  if( strncasecmp(buffer,"TRIG",4)==0 ) {
    *mode = BURST_TRIG;
  } else if( strncasecmp(buffer,"GAT",3)==0 ) {
    *mode = BURST_GAT;
  } else {
    *mode = -1;
  }
    
  return true;  

}

// ----------------------------------------------------------------------------
bool Scpi::SetTrackMode(int mode) {

  char cmd[256];

  if(!hasTrack) {
    lastError = "TRAC command not supported";
    return false;
  }

  // Connect
  if( !Connect() ) {
    return false;
  }
  
  // Send TRAC command
  switch( mode ) {

    case TRAC_ON:
      sprintf(cmd,"SOUR%d:TRAC ON\n",output);
      break;
    case TRAC_OFF:
      sprintf(cmd,"SOUR%d:TRAC OFF\n",output);
      break;
    case TRAC_INV:
      sprintf(cmd,"SOUR%d:TRAC INV\n",output);
      break;
    default:
      lastError = "Invalid track mode";
      Disconnect();
      return false;
    
  }
  
  if( !SendCommand(cmd) ) {
    Disconnect();
    return false;
  }  
  
  return SendSAV0();

}

// ----------------------------------------------------------------------------
bool Scpi::GetTrackMode(int *mode) {

  char buffer[4096];

  if(!hasTrack) {
    *mode = TRAC_OFF;
    return true;
  }

  // Connect
  if( !Connect() ) {
    return false;
  }
  
  // Send TRAC? command
  sprintf(buffer,"SOUR%d:TRAC?\n",output);
  if( !SendCommand(buffer,buffer,4096) ) {
    Disconnect();
    return false;
  }
  
  if( strncasecmp(buffer,"ON",2)==0 ) {
    *mode = TRAC_ON;
  } else if( strncasecmp(buffer,"OFF",3)==0 ) {
    *mode = TRAC_OFF;
  } else if( strncasecmp(buffer,"INV",3)==0 ) {
    *mode = TRAC_INV;
  } else {
    *mode = -1;
  }
    
  return true;  

}

// ----------------------------------------------------------------------------
bool Scpi::SetFunc(int func) {

  char cmd[256];

  // Connect
  if( !Connect() ) {
    return false;
  }
  
  // Send FUNC command

  switch( func ) {
    case FUNC_SIN:
      sprintf(cmd,"SOUR%d:FUNC SIN\n",output);
      break;
    case FUNC_SQUARE:
      sprintf(cmd,"SOUR%d:FUNC SQU\n",output);
      break;
    case FUNC_TRIANGLE:
      sprintf(cmd,"SOUR%d:FUNC TRI\n",output);
      break;
    case FUNC_RAMP:
      sprintf(cmd,"SOUR%d:FUNC RAMP\n",output);
      break;
    case FUNC_PULSE:
      sprintf(cmd,"SOUR%d:FUNC PULS\n",output);
      break;
    case FUNC_PRB:
      sprintf(cmd,"SOUR%d:FUNC PRBS\n",output);
      break;
    case FUNC_NOISE:
      sprintf(cmd,"SOUR%d:FUNC NOIS\n",output);
      break;
    case FUNC_ARB:
      sprintf(cmd,"SOUR%d:FUNC ARB\n",output);
      break;
    case FUNC_DC:
      sprintf(cmd,"SOUR%d:FUNC DC\n",output);
      break;
    default:
      lastError = "Invalid func mode";
      Disconnect();
      return false;
  }

  if( !SendCommand(cmd) ) {
    Disconnect();
    return false;
  }
  
  return SendSAV0();

}

// ----------------------------------------------------------------------------
bool Scpi::GetFunc(int *func) {

  char buffer[4096];

  // Connect
  if( !Connect() ) {
    return false;
  }
  
  // Send FUNC? command
  sprintf(buffer,"SOUR%d:FUNC?\n",output);
  if( !SendCommand(buffer,buffer,4096) ) {
    Disconnect();
    return false;
  }

  if(strncasecmp(buffer,"SIN",3)==0) {
    *func = FUNC_SIN;
  } else if(strncasecmp(buffer,"SQU",3)==0) {
    *func = FUNC_SQUARE;
  } else if(strncasecmp(buffer,"TRI",3)==0) {
    *func = FUNC_TRIANGLE;
  } else if(strncasecmp(buffer,"RAM",3)==0) {
    *func = FUNC_RAMP;
  } else if(strncasecmp(buffer,"PUL",3)==0) {
    *func = FUNC_PULSE;
  } else if(strncasecmp(buffer,"PRB",3)==0) {
    *func = FUNC_PRB;
  } else if(strncasecmp(buffer,"NOI",3)==0) {
    *func = FUNC_NOISE;
  } else if(strncasecmp(buffer,"DC",2)==0) {
    *func = FUNC_DC;
  } else if(strncasecmp(buffer,"ARB",3)==0) {
    *func = FUNC_ARB;
  } else {
    *func = -1;
  }
    
  return true;  

}

// ----------------------------------------------------------------------------
bool Scpi::SetImpedance(double ohm) {

  char cmd[256];

  // Connect
  if( !Connect() ) {
    return false;
  }
  
  // Send LOAD command
  if( ohm<=10000 ) {
    sprintf(cmd,"OUTPUT%d:LOAD %g\n",output,ohm);
  } else {
    sprintf(cmd,"OUTPUT%d:LOAD INF\n",output);
  }
  if( !SendCommand(cmd) ) {
    Disconnect();
    return false;
  }  
  
  return SendSAV0();

}

// ----------------------------------------------------------------------------
bool Scpi::GetImpedance(double *ohm) {

  char buffer[4096];

  // Connect
  if( !Connect() ) {
    return false;
  }
  
  // Send LOAD? command
  sprintf(buffer,"OUTPUT%d:LOAD?\n",output);
  if( !SendCommand(buffer,buffer,4096) ) {
    Disconnect();
    return false;
  }
  
  *ohm = atof(buffer);
  if(*ohm>10000) *ohm = POSITIVE_INF;
  
  return true;  

}

// ----------------------------------------------------------------------------
bool Scpi::SetBurstCycle(int nbCycle) {

  char cmd[256];

  // Connect
  if( !Connect() ) {
    return false;
  }
  
  // Send NCYC command
  sprintf(cmd,"SOUR%d:BURST:NCYC %d\n",output,nbCycle);
  if( !SendCommand(cmd) ) {
    Disconnect();
    return false;
  }  
  
  return SendSAV0();

}

// ----------------------------------------------------------------------------
bool Scpi::GetBurstCycle(int *nbCycle) {

  char buffer[4096];

  // Connect
  if( !Connect() ) {
    return false;
  }
  
  // Send NCYC? command
  sprintf(buffer,"SOUR%d:BURST:NCYC?\n",output);
  if( !SendCommand(buffer,buffer,4096) ) {
    Disconnect();
    return false;
  }
  
  *nbCycle = (int)(atof(buffer)+0.5);
  
  return true;  

}

// ----------------------------------------------------------------------------
bool Scpi::SetBurstState(int state) {

  char cmd[256];

  // Connect
  if( !Connect() ) {
    return false;
  }
  
  // Send BURST command
  sprintf(cmd,"SOUR%d:BURST:STAT %d\n",output,state);
  if( !SendCommand(cmd) ) {
    Disconnect();
    return false;
  }  
  
  return SendSAV0();

}

// ----------------------------------------------------------------------------
bool Scpi::GetBurstState(int *state) {

  char buffer[4096];

  // Connect
  if( !Connect() ) {
    return false;
  }
  
  // Send BURST? command
  sprintf(buffer,"SOUR%d:BURST:STAT?\n",output);
  if( !SendCommand(buffer,buffer,4096) ) {
    Disconnect();
    return false;
  }
  
  *state = atoi(buffer);
  
  return true;  

}

// ----------------------------------------------------------------------------
bool Scpi::SetOutput(int state) {

  char cmd[256];

  // Connect
  if( !Connect() ) {
    return false;
  }
  
  // Send OUTPUT command
  sprintf(cmd,"OUTPUT%d %d\n",output,state);
  if( !SendCommand(cmd) ) {
    Disconnect();
    return false;
  }  
  
  return SendSAV0();

}

// ----------------------------------------------------------------------------
bool Scpi::GetOutput(int *state) {

  char buffer[4096];

  // Connect
  if( !Connect() ) {
    return false;
  }
  
  // Send OUTPUT? command
  sprintf(buffer,"OUTPUT%d?\n",output);
  if( !SendCommand(buffer,buffer,4096) ) {
    Disconnect();
    return false;
  }
  
  *state = atoi(buffer);
  
  return true;
}

// ----------------------------------------------------------------------------
bool Scpi::SetAmplitude(double amplitude) {

  char cmd[256];

  // Connect
  if( !Connect() ) {
    return false;
  }
  
  // Send VOLT command
  sprintf(cmd,"SOUR%d:VOLT %.3f\n",output,amplitude);
  if( !SendCommand(cmd) ) {
    Disconnect();
    return false;
  }  
  
  return SendSAV0();

}

// ----------------------------------------------------------------------------
bool Scpi::GetAmplitude(double *amplitude) {

  char buffer[4096];

  // Connect
  if( !Connect() ) {
    return false;
  }
  
  // Send VOLT? command
  sprintf(buffer,"SOUR%d:VOLT?\n",output);
  if( !SendCommand(buffer,buffer,4096) ) {
    Disconnect();
    return false;
  }
  
  *amplitude = atof(buffer);
  
  return true;  

}

// ----------------------------------------------------------------------------
bool Scpi::SetOffset(double offset) {

  char cmd[256];

  // Connect
  if( !Connect() ) {
    return false;
  }
  
  // Send OFFS command
  sprintf(cmd,"SOUR%d:VOLT:OFFS %.3f\n",output,offset);
  if( !SendCommand(cmd) ) {
    Disconnect();
    return false;
  }  
  
  return SendSAV0();

}

// ----------------------------------------------------------------------------
bool Scpi::GetOffset(double *offset) {

  char buffer[4096];

  // Connect
  if( !Connect() ) {
    return false;
  }
  
  // Send OFFS? command
  sprintf(buffer,"SOUR%d:VOLT:OFFS?\n",output);
  if( !SendCommand(buffer,buffer,4096) ) {
    Disconnect();
    return false;
  }
  
  *offset = atof(buffer);
  
  return true;  
}

// ----------------------------------------------------------------------------
bool Scpi::SetFreq(double freq) {

  char cmd[256];

  // Connect
  if( !Connect() ) {
    return false;
  }
  
  // Send FREQ command
  sprintf(cmd,"SOUR%d:FREQ %g\n",output,freq);
  if( !SendCommand(cmd) ) {
    Disconnect();
    return false;
  }  
  
  return SendSAV0();

}

// ----------------------------------------------------------------------------
bool Scpi::GetFreq(double *freq) {

  char buffer[4096];

  // Connect
  if( !Connect() ) {
    return false;
  }
  
  // Send FREQ? command
  sprintf(buffer,"SOUR%d:FREQ?\n",output);
  if( !SendCommand(buffer,buffer,4096) ) {
    Disconnect();
    return false;
  }
  
  *freq = atof(buffer);
  
  return true;  

}


// ----------------------------------------------------------------------------
bool Scpi::GetNoiseBandwidth(double *bandwidth) {

  char buffer[4096];

  // Connect
  if( !Connect() ) {
    return false;
  }

  // Send SRAT? command
  sprintf(buffer,"SOUR%d:FUNC:NOIS:BAND?\n",output);
  if( !SendCommand(buffer,buffer,4096) ) {
    Disconnect();
    return false;
  }

  *bandwidth = atof(buffer);

  return true;

}

// ----------------------------------------------------------------------------
bool Scpi::SetNoiseBandwidth(double bandwidth) {

  char cmd[256];

  // Connect
  if( !Connect() ) {
    return false;
  }

  // Send FREQ command
  sprintf(cmd,"SOUR%d:FUNC:NOIS:BAND %g\n",output,bandwidth);
  if( !SendCommand(cmd) ) {
    Disconnect();
    return false;
  }

  return SendSAV0();

}

// ----------------------------------------------------------------------------
bool Scpi::SetDelay(double delay) {

  char cmd[256];

  // Connect
  if( !Connect() ) {
    return false;
  }
  
  // Send DELAY command
  sprintf(cmd,"TRIG%d:DEL %g\n",output,delay/1e6);
  if( !SendCommand(cmd) ) {
    Disconnect();
    return false;
  }  
  
  return SendSAV0();
}

// ----------------------------------------------------------------------------
bool Scpi::GetDelay(double *delay) {

  char buffer[4096];

  // Connect
  if( !Connect() ) {
    return false;
  }
  
  // Send SRAT? command
  sprintf(buffer,"TRIG%d:DEL?\n",output);
  if( !SendCommand(buffer,buffer,4096) ) {
    Disconnect();
    return false;
  }
  
  *delay = atof(buffer)*1e6;
  
  return true;
}

// ----------------------------------------------------------------------------
bool Scpi::SetOutputMode(int outputMode) {

  char cmd[256];

  // Connect
  if( !Connect() ) {
    return false;
  }

  switch (outputMode) {

    case OUTPUT_NORM:
      sprintf(cmd, "OUTP%d:MODE NORM\n", output);
      break;
    case OUTPUT_GAT:
      sprintf(cmd, "OUTP%d:MODE GAT\n", output);
      break;
    default:
      lastError = "Invalid output mode";
      Disconnect();
      return false;

  }

  if( !SendCommand(cmd) ) {
    Disconnect();
    return false;
  }

  return SendSAV0();

}

// ----------------------------------------------------------------------------
bool Scpi::GetOutputMode(int *outputMode) {

  char buffer[4096];

  // Connect
  if( !Connect() ) {
    return false;
  }

  // Send OUT:MODE? command
  sprintf(buffer,"OUTP%d:MODE?\n",output);
  if( !SendCommand(buffer,buffer,4096) ) {
    Disconnect();
    return false;
  }

  if( strncasecmp(buffer,"NORM",4)==0 ) {
    *outputMode = OUTPUT_NORM;
  } else if( strncasecmp(buffer,"GAT",3)==0 ) {
    *outputMode = OUTPUT_GAT;
  } else {
    *outputMode = -1;
  }

  return true;

}

// ----------------------------------------------------------------------------
bool Scpi::SetWaveformFilter(int waveformFilter) {

  char cmd[256];

  // Connect
  if( !Connect() ) {
    return false;
  }

  // Filter Mode
  switch (waveformFilter) {

    case FILTER_NORMAL:
      sprintf(cmd, "SOUR%d:FUNC:ARB:FILT NORM\n", output);
      break;
    case FILTER_STEP:
      sprintf(cmd, "SOUR%d:FUNC:ARB:FILT STEP\n", output);
      break;
    case FILTER_OFF:
      sprintf(cmd, "SOUR%d:FUNC:ARB:FILT OFF\n", output);
      break;
    default:
      lastError = "Invalid output mode";
      Disconnect();
      return false;

  }

  if( !SendCommand(cmd) ) {
    Disconnect();
    return false;
  }

  return SendSAV0();

}
// ----------------------------------------------------------------------------
bool Scpi::GetWaveformFilter(int *waveformFilter) {

  char buffer[4096];

  // Connect
  if( !Connect() ) {
    return false;
  }

  // Send SOUR:FUNC:ARB:FILT? command
  sprintf(buffer,"SOUR%d:FUNC:ARB:FILT?\n",output);
  if( !SendCommand(buffer,buffer,4096) ) {
    Disconnect();
    return false;
  }

  if( strncasecmp(buffer,"NORM",4)==0 ) {
    *waveformFilter = FILTER_NORMAL;
  } else if( strncasecmp(buffer,"STEP",4)==0 ) {
    *waveformFilter = FILTER_STEP;
  } else if( strncasecmp(buffer,"OFF",3)==0 ) {
    *waveformFilter = FILTER_OFF;
  } else {
    *waveformFilter = -1;
  }

  return true;


}

// ----------------------------------------------------------------------------
bool Scpi::SetSampleRate(double sampleRate) {

  char cmd[256];

  // Connect
  if( !Connect() ) {
    return false;
  }
  
  // Send SRAT command
  sprintf(cmd,"SOUR%d:FUNC:ARB:SRAT %g\n",output,sampleRate);
  if( !SendCommand(cmd) ) {
    Disconnect();
    return false;
  }  
  
  return SendSAV0();
}

// ----------------------------------------------------------------------------
bool Scpi::GetSampleRate(double *sampleRate) {

  char buffer[4096];

  // Connect
  if( !Connect() ) {
    return false;
  }
  
  // Send SRAT? command
  sprintf(buffer,"SOUR%d:FUNC:ARB:SRAT?\n",output);
  if( !SendCommand(buffer,buffer,4096) ) {
    Disconnect();
    return false;
  }
  
  *sampleRate = atof(buffer);
  
  return true;  
}

// ----------------------------------------------------------------------------
bool Scpi::SetHighVoltage(double voltage,double *newAmp,double *newOff) {

  char cmd[256];
  char buffer[4096];

  // Connect
  if( !Connect() ) {
    return false;
  }
  
  // Send VOLT command
  sprintf(cmd,"SOUR%d:VOLT:HIGH %g\n",output,voltage);
  if( !SendCommand(cmd) ) {
    Disconnect();
    return false;
  }  
  
  // Get new amplitude and offset
  sprintf(buffer,"SOUR%d:VOLT?\n",output);
  if( !SendCommand(buffer,buffer,4096) ) {
    Disconnect();
    return false;
  }
  
  *newAmp = atof(buffer);
  
  sprintf(buffer,"SOUR%d:VOLT:OFFS?\n",output);
  if( !SendCommand(buffer,buffer,4096) ) {
    Disconnect();
    return false;
  }
  
  *newOff = atof(buffer);
  
  return SendSAV0();
}

// ----------------------------------------------------------------------------
bool Scpi::GetHighVoltage(double *voltage) {

  char buffer[4096];

  // Connect
  if( !Connect() ) {
    return false;
  }
  
  // Send VOLT:HIGH? command
  sprintf(buffer,"SOUR%d:VOLT:HIGH?\n",output);
  if( !SendCommand(buffer,buffer,4096) ) {
    Disconnect();
    return false;
  }
  
  *voltage = atof(buffer);
  
  return true;
}

// ----------------------------------------------------------------------------
bool Scpi::SetLowVoltage(double voltage,double *newAmp,double *newOff) {

  char cmd[256];
  char buffer[4096];

  // Connect
  if( !Connect() ) {
    return false;
  }
  
  // Send VOLT command
  sprintf(cmd,"SOUR%d:VOLT:LOW %g\n",output,voltage);
  if( !SendCommand(cmd) ) {
    Disconnect();
    return false;
  }  

  // Get new amplitude and offset
  sprintf(buffer,"SOUR%d:VOLT?\n",output);
  if( !SendCommand(buffer,buffer,4096) ) {
    Disconnect();
    return false;
  }
  
  *newAmp = atof(buffer);
  
  sprintf(buffer,"SOUR%d:VOLT:OFFS?\n",output);
  if( !SendCommand(buffer,buffer,4096) ) {
    Disconnect();
    return false;
  }
  
  *newOff = atof(buffer);
  
  return SendSAV0();
}

// ----------------------------------------------------------------------------
bool Scpi::GetLowVoltage(double *voltage) {

  char buffer[4096];

  // Connect
  if( !Connect() ) {
    return false;
  }
  
  // Send VOLT:LOW? command
  sprintf(buffer,"SOUR%d:VOLT:LOW?\n",output);
  if( !SendCommand(buffer,buffer,4096) ) {
    Disconnect();
    return false;
  }
  
  *voltage = atof(buffer);
  
  return true;
}

// ----------------------------------------------------------------------------
bool Scpi::ReadWaveForm(int *nb,double *waveform) {

  // TO BO IMPLEMENTED
  // !!! Unfortunately volatile data cannot be uploaded !!!
  
  /*
  char buffer[65536];
  int  sock;

  // Connect
  if( !Connect() ) {
    return false;
  }
  
  // Check if the file is loaded
  if( !SendCommand("DATA:VOL:CAT?\n",buffer,65536) ) {
    Disconnect();
    return false;
  }
  
  if( strstr(buffer,"WAVEFORM")==NULL ) {
    // The file is not loaded
    lastError="No waveform loaded";
    Disconnect();
    return false;    
  }
  
  sprintf(buffer,"MMEM:UPL? WAVEFORM\n");
  nbSent = Write(sock,buffer,strlen(buffer),COMM_TIMEOUT);  
  if( nbSent<0 ) {
    Disconnect();
    return false;
  }
      
  // Read answer
  nbRead = Read(sock,buffer,65536,COMM_TIMEOUT);
  if( nbRead<0 ) {
    Disconnect();
    return false;
  }
      
  buffer[nbRead]=0;
  
  cout << "Got:" << buffer << endl;
  
  */
  
  *nb=0;
  return true;    
  
}

// ----------------------------------------------------------------------------

bool Scpi::LoadWaveFormInternal(int nb,double *waveform) {

  char *cmd = (char *)malloc(64+nb*4);

  if(cmd==NULL) {
    lastError = "Not enough memory to load the waveform";
    return false;
  }

  // Delete old waveform
  sprintf(cmd,"SOUR%d:DATA:VOL:CLEAR\n",output);
  if( !SendCommand(cmd) ) {
    free(cmd);
    return false;
  }

  // Select LSB binary format
  sprintf(cmd,"FORM:BORD SWAP\n");
  if( !SendCommand(cmd) ) {
    free(cmd);
    return false;
  }

  // Buid command string for the waveform
  int totalLength;
  char dataLength[256];
  sprintf(dataLength,"%d",nb*4);
  int ld = strlen(dataLength);
  sprintf(dataLength,"#%d%d",ld,nb*4);

  sprintf(cmd,"SOUR%d:DATA:ARB WAVEFORM%d,%s",output,output,dataLength);
  totalLength = strlen(cmd);
  float *start = (float *)(  cmd + totalLength );
      
  for(int i=0;i<nb;i++) {  
    float x = (float)waveform[i];
    SATURATE(x,-1.0f,+1.0f);
    *start = x;
    start++;
    totalLength += 4;
  }
  *(char *)start = '\n';
  totalLength++;
      
  // Send DATA
  if( !SendCommand(cmd,NULL,0,0,totalLength) ) {
    free(cmd);
    return false;
  }
  
  /*
  // Store to internal file system
  sprintf(cmd,"MMEM:STOR:DATA \"INT:\\WAVEFORM%d.arb\"\n",output);
  if( !SendCommand(cmd) ) {
    return false;  
  }

  // Reload
  sprintf(cmd,"MMEM:LOAD:DATA%d \"INT:\\WAVEFORM%d.arb\"\n",output,output);
  if( !SendCommand(cmd) ) {
    return false;  
  }
  */

  // Select ARB signal
  //sprintf(cmd,"SOUR%d:FUNC:ARB \"INT:\\WAVEFORM%d.arb\"\n",output,output);
  sprintf(cmd,"SOUR%d:FUNC:ARB WAVEFORM%d\n",output,output);
  if( !SendCommand(cmd) ) {
    free(cmd);
    return false;
  }
  
  // Select ARB mode
  sprintf(cmd,"SOUR%d:FUNC ARB\n",output);
  if( !SendCommand(cmd) ) {
    free(cmd);
    return false;
  }

  free(cmd);
  return SendSAV0();

}

// ----------------------------------------------------------------------------

bool Scpi::LoadWaveForm(int nb,double *waveform) {

  // Connect
  if( !Connect() ) {
    return false;
  }
  
  if( !LoadWaveFormInternal(nb,waveform) ) {
    Disconnect();
    return false;
  }

  return true;  
}

// ----------------------------------------------------------------------------

bool Scpi::GetIDN(string &idn) {

  char buffer[4096];

  // Connect
  if( !Connect() ) {
    return false;
  }
  
  // Send IDN command  
  if( !SendCommand((char *)"*IDN?\n",buffer,4096) ) {
    Disconnect();
    return false;
  }
 
  idn = string(buffer);
  return true;

}

// ----------------------------------------------------------------------------

bool Scpi::Reset() {

  // Connect
  if( !Connect() ) {
    return false;
  }
  
  // Send RST command  
  if( !SendCommand((char *)"*RST\n",NULL,-1) ) {
    Disconnect();
    return false;
  }
    
  // Force reconnection on reset
  Disconnect();
  return true;

}

// ----------------------------------------------------------------------------

time_t Scpi::get_ticks() {
      
    if(tickStart < 0 )
      tickStart = time(NULL);
    
    struct timeval tv;
    gettimeofday(&tv,NULL);
    return ( (tv.tv_sec-tickStart)*1000 + tv.tv_usec/1000 );

}  

// ----------------------------------------------------------------------------

string Scpi::GetLastError() {

  return lastError;

}

