//+=============================================================================
//
// file :         LoadThread.cpp
//
// description :  LoadThread class.
//                This class is used for non blocking waveform loading
//
// project :      TANGO Device Server
//
// copyleft :     European Synchrotron Radiation Facility
//                BP 220, Grenoble 38043
//                FRANCE
//
//-=============================================================================

#include "LoadThread.h"

namespace Agilent33500B_ns
{

// Constructor:
LoadThread::LoadThread(Agilent33500B *agilent33500B, omni_mutex &m):
        Tango::LogAdapter(agilent33500B), mutex(m), ds(agilent33500B)
{
  INFO_STREAM << "LoadThread::LoadThread(): entering." << endl;
  start();
}

// ----------------------------------------------------------------------------------------

void LoadThread::run(void *arg)
{

  string error = "";

  switch(ds->sequence_running) {

    case LOAD_WAVEFORM:
      if( !ds->scpi->LoadWaveForm(ds->waveFormLgth,(double *)ds->attr_Waveform_read) )
        error = ds->scpi->GetLastError() + "\n";
      if( !ds->scpi->SetAmplitude(ds->amplitudeSetpoint) )
        error += ds->scpi->GetLastError() + "\n";
      if( !ds->scpi->SetOffset(ds->offsetSetpoint) )
        error += ds->scpi->GetLastError();
      break;

    case LOAD_CONFIG:
      try {
        ds->load_configuration();
      } catch (Tango::DevFailed &e) {
        error = string(e.errors[0].desc);
      }
      break;

  }


  {
    omni_mutex_lock l(mutex);
    ds->sequence_error =  error;
    ds->sequence_running = 0;
  }

}


} // namespace Agilent33500B_ns

